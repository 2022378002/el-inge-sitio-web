// tailwind.config.js
import {nextui} from "@nextui-org/react";


/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  darkMode: "class",
  plugins: [
    nextui({
      prefix: "nextui",
      addCommonColors: false,
      defaultTheme: "light",
      defaultExtendTheme: "light",
      layout: {
      },
      themes: {
        light: {
          layout: {
            
          },
          colors: {
            background: "#F7F7F7",
            primary: {
              50: "#FFFCEA",
              100: "#FEF8D5",
              200: "#FDF1AB",
              300: "#FDEB81",
              400: "#FCE457",
              500: "#FBDD2D",
              600: "#E2C728",
              700: "#D5BC26",
              800: "#C9B124",
              900: "#BCA622",
              DEFAULT: "#FBDD2D",
            },
            secondary: {
              50: "#E8E8E8",
              100: "#D1D1D1",
              200: "#A3A3A3",
              300: "#747474",
              400: "#464646",
              500: "#181818",
              600: "#161616",
              700: "#141414",
              800: "#131313",
              900: "#121212",
              DEFAULT: "#181818",
            },
            tertiary: {
              50: "#F0F9ED",
              100: "#E2F3DB",
              200: "#C4E7B7",
              300: "#A7DA92",
              400: "#89CE6E",
              500: "#6CC24A",
              600: "#61AF43",
              700: "#5CA53F",
              800: "#569B3B",
              900: "#519237",
              DEFAULT: "#6CC24A",
            },
          }
        },
        dark: {
          layout: {},
          colors: {
            background: {
              DEFAULT: "#000000",
            },
            primary: {
              50: "#FFFCEA",
              100: "#FEF8D5",
              200: "#FDF1AB",
              300: "#FDEB81",
              400: "#FCE457",
              500: "#FBDD2D",
              600: "#E2C728",
              700: "#D5BC26",
              800: "#C9B124",
              900: "#BCA622",
              DEFAULT: "#FBDD2D",
            },
            secondary: {
              50: "#E8E8E8",
              100: "#D1D1D1",
              200: "#A3A3A3",
              300: "#747474",
              400: "#464646",
              500: "#000000",
              600: "#161616",
              700: "#141414",
              800: "#131313",
              900: "#121212",
              DEFAULT: "#000000",
            },
            tertiary: {
              50: "#F0F9ED",
              100: "#E2F3DB",
              200: "#C4E7B7",
              300: "#A7DA92",
              400: "#89CE6E",
              500: "#6CC24A",
              600: "#61AF43",
              700: "#5CA53F",
              800: "#569B3B",
              900: "#519237",
              DEFAULT: "#6CC24A",
            },
          }
        }
      }
    })
  ],
}